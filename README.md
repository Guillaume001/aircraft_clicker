# Aircraft Clicker

## Context

This project has been developed during a web project at Graduate School in Electronics, Computer Sciences,
Telecommunications, Mathematics and Mechanics.

The goal of this project is to reproduce a game similar to [CookieClicker](https://orteil.dashnet.org/cookieclicker/).

I chose to make this game on the basis of passenger transport. It is thus a question of buying pilots as well as planes
to increase the number of passengers transported.

## Installation
Execute this command on the backend and the frontend folder :
```
npm install
```
To run backend API you can execute this command :
```
cd backend && npm run start
```
To run frontend you can execute this command :
```
cd frontend && npm run serve
```

## Usage
After installation and launch backend and frontend, you can access to two addresses :
* API link : [http://localhost:3000](http://localhost:3000)
* Frontend link : [http://localhost:8080](http://localhost:8080)

## Default accounts
There are two accounts by default :
* Username: alice / Password: password
* Username: bob / Password: password

__Alice account has a game saved already in order to test.__

## Know issues

### Frontend
* The navigation bar not displaying correctly on iOS devices
* When switching page, there are some problems wih asynchronous functions

### Backend
* Authentication system is not secure

## Possible improvements
* Put a different image for powers
* Can put the local save on a specific user
* Manage users page to delete or modify current users
* Add a vertical scroll bar on the second and third column
* The score page is not representative of the user's actual score, so a comparative tool should be added to compare all the statistics of all users

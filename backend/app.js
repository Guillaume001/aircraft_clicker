const express = require('express');
const logger = require('morgan');
const cors = require("cors");
const Sequelize = require("sequelize");

const app = express();

app.use(cors());
app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));

// Sequelizer
const sequelize = new Sequelize("sqlite:database.db");

const User = sequelize.define("user", {
    username: {
        type: Sequelize.STRING,
        unique: true,
        allowNull: false
    },
    password: {
        type: Sequelize.STRING,
        allowNull: false
    }
});

const Game = sequelize.define("game", {
    counter: {
        type: Sequelize.INTEGER,
        default: 0,
        allowNull: false
    },
    export_time: {
        type: Sequelize.DATE,
        default: Date.now(),
        allowNull: false
    }
}, {timestamps: false});

const Item = sequelize.define("item", {
    ig_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    counter: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    unlock: {
        type: Sequelize.BOOLEAN,
        default: false
    },
    unitsShow: {
        type: Sequelize.BOOLEAN,
        default: true
    }
}, {timestamps: false});

const Power = sequelize.define("power", {
    ig_id: {
        type: Sequelize.INTEGER,
        allowNull: false
    },
    buy: {
        type: Sequelize.BOOLEAN,
        default: false
    }
}, {timestamps: false});

User.hasOne(Game, {onDelete: 'CASCADE'});
Game.belongsTo(User);
Game.hasMany(Item, {onDelete: 'CASCADE'});
Item.belongsTo(Game);
Item.hasMany(Power, {onDelete: 'CASCADE'});
Power.belongsTo(Item);

User.sync({force: true}).then(() => {
    const example_game = require('./example_backup_game.json');
    User.create({
            id: 1,
            username: "alice",
            password: "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8",
            game: example_game
        }, {
            include: [{
                association: User.Game,
                model: Game,
                include: [{
                    association: Game.Item,
                    model: Item,
                    include: [{
                        association: Item.Power,
                        model: Power
                    }]
                }
                ]
            }]
        }
    );
    User.create({
        id: 2,
        username: "bob",
        password: "5e884898da28047151d0e56f8dc6292773603d0d6aabbdd62a11ef721d1542d8"
    });
});
Game.sync({force: true});
Item.sync({force: true});
Power.sync({force: true});

app.get('/', function (req, res) {
    res.status(200).json({code: 200, message: 'API is ready'});
});

app.get('/score', function (req, res) {
    User.findAll({include: Game}).then((users => {
        let usersFormatted = [];
        users.map(u => {
            let us = {id: u.id, username: u.username, game: null};
            if (u.game) {
                us.game = {id: u.game.id, counter: u.game.counter};
            }
            usersFormatted.push(us);
        });
        res.status(200).json({code: 200, users: usersFormatted});
    }));
});

app.post('/login', function (req, res) {
    const username = req.body.username;
    const hashedPassword = req.body.hashedPassword;
    if (!username || !hashedPassword) {
        res.status(400).json({code: 400, message: 'Request is not correct'});
        return;
    }
    User.findAll({
        where: {
            username: username,
            password: hashedPassword
        }
    }).then(users => {
        if (users.length > 0) {
            res.status(200).json({code: 200, message: 'User connected', userConnected: users[0]});
        } else {
            res.status(200).json({code: 403, message: 'User is not connected', userConnected: null});
        }
    });
});

app.post('/subscribe', function (req, res) {
    const username = req.body.username;
    const hashedPassword = req.body.hashedPassword;
    if (!username || !hashedPassword) {
        res.status(400).json({code: 400, message: 'Request is not correct'});
        return;
    }
    User.create({
        username: username,
        password: hashedPassword
    }).then(user =>
        res.status(200).json({code: 200, message: 'User created', user: user})
    ).catch(error =>
        res.status(200).json({code: 403, message: 'Error during create user', user: null})
    );
});

app.get('/game/:user_id', function (req, res) {
    Game.findOne({
        where: {userId: req.params.user_id}, include: [{
            association: Game.Item,
            model: Item,
            include: [{association: Item.Power, model: Power}]
        }]
    }).then((game) => {
        if (game === null) {
            res.json({code: 200, game: null});
            return;
        }
        let save = {};
        save.counter = game.counter;
        save.exportTime = new Date(game.export_time).getTime();
        save.items = [];
        game.items.map(i => {
            let item = {id: i.ig_id, counter: i.counter, unlock: i.unlock, unitsShow: i.unitsShow, powers: []};
            i.powers.map(p => {
                item.powers.push({id: p.ig_id, buy: p.buy});
            })
            save.items.push(item);
        });
        res.json({code: 200, game: save});
    });
});

app.post('/game/:user_id/save', function (req, res) {
    Game.findOne({where: {userId: req.params.user_id}}).then(game => {
        if (game !== null)
            game.destroy();
        let save = req.body;
        Game.create({
            userId: req.params.user_id,
            counter: save.counter,
            export_time: new Date(save.exportTime)
        }).then(game => {
            let items = [];
            let powers = [];
            save.items.map(i => {
                items.push({
                    gameId: game.id,
                    ig_id: i.id,
                    counter: i.counter,
                    unlock: i.unlock,
                    unitsShow: i.unitsShow
                });
                i.powers.map(p => {
                    powers.push({ig_id: p.id, buy: p.buy, item_ig_id: i.id});
                });
            });
            Item.bulkCreate(items).then((items => {
                powers.map(p => {
                    let itemBdd = items.find(i => i.ig_id === p.item_ig_id);
                    delete p.item_ig_id;
                    p.itemId = itemBdd.id;
                })
                Power.bulkCreate(powers).then((powers) => {
                    res.json({
                        code: 200,
                        message: 'Game successfully saved'
                    });
                })
            }));
        }).catch(error => res.json({code: 500, message: 'Game cannot be saved'}));
    });
});

module.exports = app;

if (!module.parent) {
    app.listen(3000);
    console.log('Express started on port 3000');
}

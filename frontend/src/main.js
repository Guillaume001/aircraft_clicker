import Vue from 'vue'
import App from './App.vue'
import router from './router'
import BootstrapVue from "bootstrap-vue";
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import {library} from '@fortawesome/fontawesome-svg-core'
import {
    faEye,
    faEyeSlash,
    faGamepad,
    faInfoCircle,
    faKey,
    faListOl,
    faPlane,
    faTools,
    faUser,
    faWarehouse
} from '@fortawesome/free-solid-svg-icons'
import {FontAwesomeIcon} from '@fortawesome/vue-fontawesome'

library.add(faUser, faKey, faEyeSlash, faTools, faWarehouse, faPlane, faEye, faGamepad, faInfoCircle, faListOl)
Vue.component('font-awesome-icon', FontAwesomeIcon)

Vue.use(BootstrapVue);

Vue.config.productionTip = false;

Vue.prototype.$apiUrl = 'http://localhost:3000';

new Vue({
    router,
    render: h => h(App)
}).$mount('#app')

import Vue from 'vue'
import VueRouter from 'vue-router'
import Game from '../views/Game.vue'
import Login from '../views/Login.vue'
import Logout from "@/views/Logout";
import Register from "@/views/Register";
import Score from "@/views/Score";
import About from "@/views/About";

Vue.use(VueRouter)

const routes = [
    {
        path: '/',
        name: 'Game',
        component: Game,
    },
    {
        path: '/login',
        name: 'Login',
        component: Login,
    },
    {
        path: '/score',
        name: 'Score',
        component: Score,
    },
    {
        path: '/about',
        name: 'About',
        component: About,
    },
    {
        path: '/subscribe',
        name: 'Register',
        component: Register,
    },
    {
        path: '/logout',
        name: 'Logout',
        component: Logout
    }
]

const router = new VueRouter({
    routes
})

export default router

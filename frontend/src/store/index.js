import Vue from "vue";
import Vuex from "vuex";

Vue.use(Vuex);

export default new Vuex.Store({
    state: {
        online: false,
        user: null,
    },
    mutations: {
        SET_ONLINE: function (state, online) {
            state.online = online;
        },
        SET_USER: function (state, user) {
            state.user = user;
        }
    },
    actions: {
        setOnline(context, online) {
            context.commit('SET_ONLINE', online);
        },
        setUserConnected(context, user) {
            context.commit('SET_USER', user);
            if (user === null) localStorage.setItem('user', null);
            else localStorage.setItem('user', JSON.stringify(user));
        }
    },
    getters: {
        isOnline: state => {
            return state.online;
        },
        user: state => {
            return state.user;
        },
        loggedIn: state => {
            return state.user !== null;
        }
    }
});

export default class Item {

    id;
    name;
    unlock = false;
    unitsShow = true;
    counter = 0;
    icon;
    powers = [];
    baseCost;
    coefficientPerSecond;

    cost = function () {
        return Math.round(this.baseCost * Math.pow(1.15, this.counter));
    }

    costIfBuyQuantity = function (quantity) {
        let price = this.cost();
        for (let i = 1; i < quantity; i++) {
            price += Math.round(this.baseCost * Math.pow(1.15, this.counter + i));
        }
        return price;
    }

    costIfBuyQuantityFormatted = function (quantity) {
        return Intl.NumberFormat("fr-FR", {
            notation: "compact",
            compactDisplay: "short",
            maximumFractionDigits: 3,
        }).format(this.costIfBuyQuantity(quantity));
    }

    generatePerSecond = function () {
        let power = this.powers.reduce(function (sum, power) {
            if (power.buy)
                return sum + power.upgrade.perSecondByCounter;
            return sum;
        }, 0);
        if (power === 0)
            power = 1;
        return power * this.counter * this.coefficientPerSecond;
    }

    constructor(id, name, icon, baseCost, coefficientPerSecond) {
        this.id = id;
        this.name = name;
        this.icon = icon;
        this.coefficientPerSecond = coefficientPerSecond;
        this.baseCost = baseCost;
    }

}

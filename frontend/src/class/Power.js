export default class Power {
    id;
    name;
    description = function () {
        let description = '';

        if (this.upgrade.perSecondByCounter !== 0)
            description += 'Augmente de ' + Math.round((this.upgrade.perSecondByCounter - 1) * 100) + '% l\'efficacité par seconde.<br>';

        return description;
    };
    buy = false;
    cost;

    costFormatted = function () {
        return Intl.NumberFormat("fr-FR", {
            notation: "compact",
            compactDisplay: "short",
            maximumFractionDigits: 3,
        }).format(this.cost);
    }

    upgrade = {
        perSecondByCounter: 0,
        perClicker: 0,
        perClickerByCounter: 0
    };

    previousPower = -1;
    unlockCondition;

    constructor(id, name, previousPower, unlockCondition, cost, perSecondByCounter, perClicker, perClickerByCounter) {
        this.id = id;
        this.name = name;
        this.previousPower = previousPower;
        this.unlockCondition = unlockCondition;
        this.cost = cost;
        this.upgrade.perSecondByCounter = perSecondByCounter;
        this.upgrade.perClicker = perClicker;
        this.upgrade.perClickerByCounter = perClickerByCounter;
    }

}
